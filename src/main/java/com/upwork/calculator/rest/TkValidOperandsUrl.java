package com.upwork.calculator.rest;

import com.jcabi.log.Logger;
import java.io.IOException;
import java.net.HttpURLConnection;
import lombok.EqualsAndHashCode;
import org.takes.Response;
import org.takes.facets.fork.RqRegex;
import org.takes.facets.fork.TkRegex;
import org.takes.rs.RsWithBody;
import org.takes.rs.RsWithStatus;

/**
 * A decorator for {@link TkRegex} that ensures the validity of url arguments
 * part. Keeps both validation checks and reaction logic in case of invalid
 * arguments passed.
 *
 * @author Eugene Kondrashev (eugene.kondrashev)
 * @version $Id$
 * @since 0.1
 */
@EqualsAndHashCode(of = "origin")
public final class TkValidOperandsUrl implements TkRegex {

    /**
     * Original Take.
     */
    private final transient TkRegex origin;

    /**
     * Names to validate.
     */
    private final transient String[] names;

    /**
     * Ctor.
     *
     * @param origin Original Take
     * @param names To validate
     */
    public TkValidOperandsUrl(final TkRegex origin, final String... names) {
        this.origin = origin;
        this.names = names;
    }

    @Override
    public Response act(final RqRegex req) throws IOException {
        for (final String name : this.names) {
            final String value = req.matcher().group(name);
            if (!TkValidOperandsUrl.valid(value)) {
                final String message = String.format(
                    "invalid operand %s", value
                );
                Logger.error(this, message);
                return new RsWithStatus(
                    new RsWithBody(message), HttpURLConnection.HTTP_BAD_REQUEST
                );
            }
        }
        return this.origin.act(req);
    }

    /**
     * Validates an operand. Operand should be a Number.
     *
     * @param operand To validate.
     * @return True if operand is valid.
     */
    private static boolean valid(final String operand) {
        boolean valid = true;
        try {
            Double.parseDouble(operand);
        } catch (final NumberFormatException ex) {
            valid = false;
        }
        return valid;
    }

}
