package com.upwork.calculator.rest;

import com.upwork.calculator.operations.Operation;
import org.takes.Response;
import org.takes.facets.fork.RqRegex;
import org.takes.facets.fork.TkRegex;
import org.takes.rs.RsJSON;
import org.takes.rs.RsPrettyJSON;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Specific operation endpoint handler binding arithmetic implementation with
 * url path.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
public class TkOperation implements TkRegex {
    /**
     * Arithmetical operation implementation.
     */
    private final transient Operation<Double> operation;

    /**
     * Names of operation argumetes to be parsed from url.
     */
    private final transient String[] names;

    /**
     * Ctor.
     * @param operation Implementation to use while reacting on HTTP request
     * @param names Of an operands to parse from url
     */
    public TkOperation(final Operation<Double> operation, final String... names) {
        this.operation = operation;
        this.names = names;
    }

    @Override
    public Response act(RqRegex req) throws IOException {
        final List<Double> operands = new LinkedList<Double>();
        for (final String name : this.names) {
            operands.add(Double.parseDouble(req.matcher().group(name)));
        }
        return new RsPrettyJSON(
            new RsJSON(
                new Operation.ToJson(operation)
                    .call(operands.toArray(new Double[operands.size()]))
            )
        );
    }

}
