package com.upwork.calculator.rest;

import com.upwork.calculator.operations.Operation;
import org.takes.facets.fork.FkRegex;
import org.takes.facets.fork.FkWrap;
import org.takes.facets.fork.Fork;

/**
 * Generic fork arithmetical operation, dispatching request to particular
 * operation implemetation.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
public class FkOperation extends FkWrap {
    /**
     * Ctor.
     * @param source Operation to forward request to
     * @param names Of the operands
     */
    public FkOperation(final Operation<Double> source,
        final String... names) {
        super(FkOperation.create(source, names));
    }

    /**
     * Creates a fork based on regular expression, like
     * /operation/(?<a>.+)/(?<b>.+)/(?<c>.+),
     * @param operation To create fork for
     * @param names Of operands to expect for this endpoint
     * @return Operation fork with template endpoint
     */
    private static Fork create(final Operation<Double> operation,
        final String... names) {
        final StringBuilder template = new StringBuilder();
        template.append('/');
        template.append(operation.name());
        for (final String name : names) {
            template.append('/');
            template.append(String.format("(?<%s>.+)", name));
        }
        return new FkRegex(
            template.toString(),
            new TkValidOperandsUrl(
                new TkOperation(
                    operation,
                    names
                ),
                names
            )
        );
    }
}
