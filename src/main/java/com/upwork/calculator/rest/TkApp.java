package com.upwork.calculator.rest;

import com.upwork.calculator.operations.Calculator;
import com.upwork.calculator.operations.Cache;
import lombok.EqualsAndHashCode;
import org.takes.Take;
import org.takes.facets.fork.TkFork;
import org.takes.tk.TkWrap;

/**
 * Take application, registering required operation forks.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
@EqualsAndHashCode(callSuper = true)
public final class TkApp extends TkWrap {

    /**
     * List of argument names for add, substract and multiply operations.
     */
    private static final String[] ABC = { "a", "b", "c" };
    /**
     * List of argument names for divide operation.
     */
    private static final String[] AB = { "a", "b", };

    /**
     * Ctor.
     */
    public TkApp() {
        this(
            new Calculator.Caching(
                new Calculator.Default(), new Cache.Default()
            )
        );
    }

    /**
     * Ctor.
     * @param calculator To use wile creating specific arithmetical operations
     */
    public TkApp(final Calculator calculator) {
        super(TkApp.regex(calculator));
    }

    /**
     * Regex takes.
     *
     * @return Takes
     */
    private static Take regex(final Calculator calculator) {
        return new TkFork(
            new FkOperation(calculator.add(), TkApp.ABC),
            new FkOperation(calculator.substract(), TkApp.ABC),
            new FkOperation(calculator.multiply(), TkApp.ABC),
            new FkOperation(calculator.divide(), TkApp.AB)
        );
    }
}
