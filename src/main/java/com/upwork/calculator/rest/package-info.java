/**
 * This package contains REST endpoint handlers, url argument validator,
 * fork operations and entry point Take application implementation tying it all
 * together.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
package com.upwork.calculator.rest;
