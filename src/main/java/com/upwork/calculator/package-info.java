/**
 * Package containing entry point for the REST based calulator.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
package com.upwork.calculator;
