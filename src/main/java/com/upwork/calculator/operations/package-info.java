/**
 * This package provides interface for arithmetical operation along with basic
 * implementations.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.3
 */
package com.upwork.calculator.operations;
