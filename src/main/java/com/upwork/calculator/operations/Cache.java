package com.upwork.calculator.operations;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Operation cache.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
public interface Cache {

    /**
     * Cache accessor. This method will update a cache in case current
     *  operation with provided arguments was not cached yet.
     * @param operation To search in the cache
     * @param args Of an operation
     * @return Cached value
     */
    Double get(Operation<Double> operation, Double... args);

    /**
     * Cache key, represented by operation name and it arguments.
     */
    final class Key {
        /**
         * Operation name.
         */
        private final transient String operation;
        /**
         * Operation arguments.
         */
        private final transient Double[] operands;

        /**
         * Ctor.
         * @param operation To create key for
         * @param operands Of passed operation
         */
        public Key(final Operation<Double> operation,
            final Double... operands) {
            this.operation = operation.name();
            this.operands = operands;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || this.getClass() != o.getClass())
                return false;

            Cache.Key key = (Cache.Key) o;

            if (!Arrays.equals(this.operands, key.operands))
                return false;
            if (!this.operation.equals(key.operation))
                return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = this.operation.hashCode();
            result = 31 * result + Arrays.hashCode(this.operands);
            return result;
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append(this.operation);
            builder.append("->");
            for (final Double arg : this.operands) {
                builder.append(arg.toString());
                builder.append(", ");
            }
            return builder.toString();
        }
    }

    /**
     * Default cache implementation backed with a map.
     */
    final class Default implements Cache {
        /**
         * Backend storage.
         */
        private final transient Map<Cache.Key, Double> store;

        /**
         * Ctor.
         * @param store To use as backend
         */
        public Default(final Map<Cache.Key, Double> store) {
            this.store = store;
        }

        /**
         * Ctor.
         */
        public Default() {
            this(new ConcurrentHashMap<Key, Double>());
        }

        @Override
        public Double get(Operation<Double> operation,
            Double... args) {
            final Key key = new Key(operation, args);
            Double result = this.store.get(key);
            if (result == null) {
                result = operation.call(args);
                this.store.put(key, result);
            }
            return result;
        }
    }

}
