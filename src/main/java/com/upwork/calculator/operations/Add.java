package com.upwork.calculator.operations;

/**
 * Add operation implementation.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
public class Add implements Operation {

    @Override
    public Double call(final Double... operands) {
        double result = 0;
        for (final Double operand: operands) {
            result += operand;
        }
        return result;
    }

    @Override
    public String name() {
        return "add";
    }

}
