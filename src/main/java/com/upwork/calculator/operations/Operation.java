package com.upwork.calculator.operations;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonStructure;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Arithmetic operation abstraction.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
public interface Operation<T> {
    /**
     * Operation name accessor.
     *
     * @return Operation name
     */
    String name();

    /**
     * Triggers current operation execution against passed arguments.
     *
     * @param args To use during operation calculation
     * @return Result of operation execution
     */
    T call(Double... args);

    /**
     * Operation with caching logic.
     */
    final class Caching implements Operation<Double> {

        /**
         * Cache to use.
         */
        private final transient Cache cache;
        /**
         * Original operation to decorate with caching logic.
         */
        private final transient Operation<Double> origin;

        /**
         * Ctor.
         *
         * @param origin Operation to decorate with caching logic
         * @param cache  To use
         */
        public Caching(final Operation<Double> origin,
            final Cache cache) {
            this.origin = origin;
            this.cache = cache;
        }

        @Override
        public String name() {
            return this.origin.name();
        }

        @Override
        public Double call(final Double... args) {
            return this.cache.get(this.origin, args);
        }
    }

    /**
     * Operation, producing json structure as a result of its execution.
     */
    final class ToJson implements Operation<JsonStructure> {
        /**
         * Origin operation to create results of a calculation that will
         * be included to the output json.
         */
        private final transient Operation<Double> source;

        /**
         * Ctor.
         *
         * @param operation To use to get the details of real operation and get
         *                  the results.
         */
        public ToJson(final Operation<Double> operation) {
            this.source = operation;
        }

        @Override
        public String name() {
            return this.source.name();
        }

        @Override
        public javax.json.JsonStructure call(final Double... args) {
            final JsonArrayBuilder arguments = Json.createArrayBuilder();
            for (final Double arg : args) {
                arguments.add(arg);
            }
            return Json.createObjectBuilder()
                .add(
                    "operation", Json.createObjectBuilder()
                        .add("name", this.source.name())
                        .add("arguments", arguments)
                        .add("result", this.source.call(args))
                )
                .build();
        }
    }

    /**
     * Operation with counting logic.
     */
    final class Counting implements Operation<Double> {

        /**
         * Original operation to track executions count.
         */
        private final transient Operation<Double> origin;

        /**
         * Counter.
         */
        private final transient AtomicInteger count;

        /**
         * Ctor.
         *
         * @param origin Operation to decorate
         * @param count  To use
         */
        public Counting(final Operation<Double> origin,
            final AtomicInteger count) {
            this.origin = origin;
            this.count = count;
        }

        @Override
        public String name() {
            return origin.name();
        }

        @Override
        public Double call(Double... args) {
            this.count.incrementAndGet();
            return this.origin.call(args);
        }
    }
}
