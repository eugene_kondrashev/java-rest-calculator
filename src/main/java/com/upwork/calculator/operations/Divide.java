package com.upwork.calculator.operations;

/**
 * Divide arithmetic operation implementation.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
public class Divide implements Operation {

    @Override
    public Double call(final Double... operands) {
        double result = operands[0];
        for (int i = 1; i < operands.length; i++) {
            result /= operands[i];
        }
        return result;
    }

    @Override
    public String name() {
        return "divide";
    }

}
