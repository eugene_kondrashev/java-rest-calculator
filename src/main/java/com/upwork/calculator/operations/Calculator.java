package com.upwork.calculator.operations;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Calculator providing base arithmetical operations.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.3
 */
public interface Calculator {
    /**
     * Add operation.
     * @return Operation with add logic.
     */
    Operation add();

    /**
     * Substract operation.
     * @return Operation with substract logic
     */
    Operation substract();

    /**
     * Multiply operation.
     * @return Operation with multiply logic
     */
    Operation multiply();

    /**
     * Divide operation.
     * @return Operation with divide logic.
     */
    Operation divide();

    /**
     * Defaulr implementation of a calculator.
     */
    final class Default implements Calculator {

        @Override
        public Operation add() {
            return new Add();
        }

        @Override
        public Operation substract() {
            return new Substruct();
        }

        @Override
        public Operation multiply() {
            return new Multiply();
        }

        @Override
        public Operation divide() {
            return new Divide();
        }
    }

    /**
     * Calculator providing caching operations.
     */
    final class Caching implements Calculator {
        /**
         * Origin calculator providing operations to decorate with caching logic.
         */
        private final transient Calculator origin;
        /**
         * Cache storage.
         */
        private final transient Cache cache;

        /**
         * Ctor.
         * @param origin Calculator providing operations to decorate with
         *  caching logic
         */
        public Caching(final Calculator origin) {
            this(origin, new Cache.Default());
        }

        /**
         * Ctor.
         * @param origin Calculator providing operations to decorate with
         *  caching logic
         * @param cache Storage
         */
        public Caching(final Calculator origin, final Cache cache) {
            this.origin = origin;
            this.cache = cache;
        }

        @Override
        public Operation add() {
            return new Operation.Caching(this.origin.add(), this.cache);
        }

        @Override
        public Operation substract() {
            return new Operation.Caching(this.origin.substract(), this.cache);
        }

        @Override
        public Operation multiply() {
            return new Operation.Caching(this.origin.multiply(), this.cache);
        }

        @Override
        public Operation divide() {
            return new Operation.Caching(this.origin.divide(), this.cache);
        }
    }

    /**
     * Calculator decorating original operations with counting capabilities.
     */
    final class Counting implements Calculator {

        /**
         * Add operations counter
         */
        private final transient AtomicInteger adds = new AtomicInteger();
        /**
         * Substract operations counter.
         */
        private final transient AtomicInteger substracts = new AtomicInteger();
        /**
         * Multiply operations counter
         */
        private final transient AtomicInteger muliplies = new AtomicInteger();
        /**
         * Divide operations counter
         */
        private final transient AtomicInteger divides = new AtomicInteger();

        /**
         * Origin calculator providing operations to decorate with counting logic.
         */
        private final transient Calculator origin;

        /**
         * Ctor.
         * @param origin Calculator providing operations to decorate with
         *  counting logic.
         */
        public Counting(final Calculator origin) {
            this.origin = origin;
        }

        @Override
        public Operation add() {
            return new Operation.Counting(this.origin.add(), this.adds);
        }

        @Override
        public Operation substract() {
            return new Operation.Counting(
                this.origin.substract(), this.substracts
            );
        }

        @Override
        public Operation multiply() {
            return new Operation.Counting(
                this.origin.multiply(), this.muliplies
            );
        }

        @Override
        public Operation divide() {
            return new Operation.Counting(this.origin.divide(), this.divides);
        }

        /**
         * Add operations amount accessor.
         * @return Amount of add operations performed.
         */
        public int adds() {
            return this.adds.get();
        }

        /**
         * Substract operations amount accessor.
         * @return Amount of substaract operations performed.
         */
        public int substracts() {
            return this.substracts.get();
        }

        /**
         * Multiply operations amount accessor.
         * @return Amount of multiply operations performed.
         */
        public int multiplies() {
            return this.muliplies.get();
        }

        /**
         * Divide operations amount accessor.
         * @return Amount of divide operations performed.
         */
        public int divides() {
            return this.divides.get();
        }
    }
}
