package com.upwork.calculator;

import com.upwork.calculator.rest.TkApp;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import org.takes.http.Exit;
import org.takes.http.FtCLI;
import java.io.IOException;

/**
 * Entry point to the app.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.1
 */
public final class Main {

    public static void main(final String[] args) throws IOException {
        final OptionParser parser = new OptionParser();
        parser.allowsUnrecognizedOptions();
        final OptionSpec<String> port =
            parser.accepts("port").withRequiredArg().required();
        final OptionSet options = parser.parse(args);
        if (options.valueOf(port).equals("0")) {
            throw new IllegalArgumentException("Invalid port value 0");
        }
        new FtCLI(new TkApp(), args).start(Exit.NEVER);
    }
}
