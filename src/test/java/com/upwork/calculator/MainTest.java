package com.upwork.calculator;

import com.jayway.awaitility.Awaitility;
import com.jayway.awaitility.Duration;
import com.jcabi.http.request.JdkRequest;
import com.jcabi.http.response.JsonResponse;
import com.upwork.calculator.operations.Calculator;
import com.upwork.calculator.rest.TkApp;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.takes.http.Exit;
import org.takes.http.FtCLI;
import org.takes.http.MainRemote;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;

/**
 * Test case for {@link Main}.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.3
 */
public class MainTest {
    /**
     * Main can perform add operation.
     *
     * @throws Exception In case of error.
     */
    @Test
    public void performsAddOperation() throws Exception {
        new MainRemote(Main.class).exec(
            new MainRemote.Script() {
                @Override
                public void exec(final URI uri) throws IOException {
                    MainTest.assertOperation(uri, "add", 6.0, 1.0, 2.0, 3.0);
                }
            }
        );
    }

    /**
     * Main can perform substract operation.
     *
     * @throws Exception In case of error.
     */
    @Test
    public void performsSubstractOperation() throws Exception {
        new MainRemote(Main.class).exec(
            new MainRemote.Script() {
                @Override
                public void exec(final URI uri) throws IOException {
                    MainTest.assertOperation(uri, "substract", -4.0, 1.0, 2.0, 3.0);
                }
            }
        );
    }

    /**
     * Main can perform multiply operation.
     *
     * @throws Exception In case of error.
     */
    @Test
    public void performsMultiplyOperation() throws Exception {
        new MainRemote(Main.class).exec(
            new MainRemote.Script() {
                @Override
                public void exec(final URI uri) throws IOException {
                    MainTest.assertOperation(uri, "multiply", 6.0, 1.0, 2.0, 3.0);
                }
            }
        );
    }

    /**
     * Main can perform divide operation.
     *
     * @throws Exception In case of error.
     */
    @Test
    public void performsDivideOperation() throws Exception {
        new MainRemote(Main.class).exec(
            new MainRemote.Script() {
                @Override
                public void exec(final URI uri) throws IOException {
                    MainTest.assertOperation(uri, "divide", 0.5, 1.0, 2.0);
                }
            }
        );
    }

    /**
     * Main can make use of cache on second divide operation.
     *
     * @throws Exception In case of error.
     */
    @Test
    public void makesUseOfCacheOnSecondDivideOperation() throws Exception {
        final URI uri = MainTest.uri();
        final Calculator.Counting dflt = new Calculator.Counting(
            new Calculator.Default()
        );
        final Calculator.Counting caching = new Calculator.Counting(
            new Calculator.Caching(dflt)
        );
        final Thread server = MainTest.startServer(uri, caching);

        try {
            MainTest.assertOperation(uri, "divide", 0.5, 1.0, 2.0);
            MatcherAssert.assertThat(dflt.divides(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.divides(), Matchers.equalTo(1));
            MainTest.assertOperation(uri, "divide", 0.5, 1.0, 2.0);
            MatcherAssert.assertThat(dflt.divides(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.divides(), Matchers.equalTo(2));
            MainTest.assertOperation(uri, "divide", 0.5, 1.0, 2.0);
            MatcherAssert.assertThat(dflt.divides(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.divides(), Matchers.equalTo(3));
        } finally {
            server.interrupt();
        }
    }

    /**
     * Main can make use of cache on second multiply operation.
     *
     * @throws Exception In case of error.
     */
    @Test
    public void makesUseOfCacheOnSecondMultiplyOperation() throws Exception {
        final URI uri = MainTest.uri();
        final Calculator.Counting dflt = new Calculator.Counting(
            new Calculator.Default()
        );
        final Calculator.Counting caching = new Calculator.Counting(
            new Calculator.Caching(dflt)
        );
        final Thread server = MainTest.startServer(uri, caching);

        try {
            MainTest.assertOperation(uri, "multiply", 6.0, 1.0, 2.0, 3.0);
            MatcherAssert.assertThat(dflt.multiplies(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.multiplies(), Matchers.equalTo(1));
            MainTest.assertOperation(uri, "multiply", 6.0, 1.0, 2.0, 3.0);
            MatcherAssert.assertThat(dflt.multiplies(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.multiplies(), Matchers.equalTo(2));
            MainTest.assertOperation(uri, "multiply", 6.0, 1.0, 2.0, 3.0);
            MatcherAssert.assertThat(dflt.multiplies(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.multiplies(), Matchers.equalTo(3));
        } finally {
            server.interrupt();
        }
    }
    /**
     * Main can make use of cache on second add operation.
     *
     * @throws Exception In case of error.
     */
    @Test
    public void makesUseOfCacheOnSecondAddOperation() throws Exception {
        final URI uri = MainTest.uri();
        final Calculator.Counting dflt = new Calculator.Counting(
            new Calculator.Default()
        );
        final Calculator.Counting caching = new Calculator.Counting(
            new Calculator.Caching(dflt)
        );
        final Thread server = MainTest.startServer(uri, caching);

        try {
            MainTest.assertOperation(uri, "add", 3.0, 1.0, 2.0, 0.0);
            MatcherAssert.assertThat(dflt.adds(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.adds(), Matchers.equalTo(1));
            MainTest.assertOperation(uri, "add", 3.0, 1.0, 2.0, 0.0);
            MatcherAssert.assertThat(dflt.adds(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.adds(), Matchers.equalTo(2));
            MainTest.assertOperation(uri, "add", 3.0, 1.0, 2.0, 0.0);
            MatcherAssert.assertThat(dflt.adds(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.adds(), Matchers.equalTo(3));
        } finally {
            server.interrupt();
        }
    }

    /**
     * Main can make use of cache on second subtract operation.
     *
     * @throws Exception In case of error.
     */
    @Test
    public void makesUseOfCacheOnSecondSubtractOperation() throws Exception {
        final URI uri = MainTest.uri();
        final Calculator.Counting dflt = new Calculator.Counting(
            new Calculator.Default()
        );
        final Calculator.Counting caching = new Calculator.Counting(
            new Calculator.Caching(dflt)
        );
        final Thread server = MainTest.startServer(uri, caching);

        try {
            MainTest.assertOperation(uri, "substract", -1.0, 1.0, 2.0, 0.0);
            MatcherAssert.assertThat(dflt.substracts(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.substracts(), Matchers.equalTo(1));
            MainTest.assertOperation(uri, "substract", -1.0, 1.0, 2.0, 0.0);
            MatcherAssert.assertThat(dflt.substracts(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.substracts(), Matchers.equalTo(2));
            MainTest.assertOperation(uri, "substract", -1.0, 1.0, 2.0, 0.0);
            MatcherAssert.assertThat(dflt.substracts(), Matchers.equalTo(1));
            MatcherAssert.assertThat(caching.substracts(), Matchers.equalTo(3));
        } finally {
            server.interrupt();
        }
    }
    private static Thread startServer(final URI uri, final Calculator calc) {
        final Thread server = new Thread() {
            public void run() {
                try {
                    new FtCLI(
                        new TkApp(calc),
                        new String[] {
                            String.format("--port=%d", uri.getPort())
                        }
                    )
                        .start(Exit.NEVER);
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new IllegalStateException(e);
                }
            }
        };
        server.start();
        Awaitility.await().atMost(Duration.FIVE_SECONDS).until(
            new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    boolean ended;
                    try (
                        final Socket skt = new Socket()) {
                        skt.connect(
                            new InetSocketAddress(
                                InetAddress.getLocalHost(), uri.getPort()
                            )
                        );
                        ended = true;
                    } catch (final FileNotFoundException ignored) {
                        ended = false;
                    }
                    return ended;
                }
            }
        );
        return server;
    }

    /**
     * Asserts operation execution.
     * @param base Uri to use while calling REST endpoint
     * @param operation To call
     * @param result To expect
     * @param args To pass to the operation
     * @throws IOException In case of cummunication issues
     */
    private static void assertOperation(final URI base, final String operation,
        final Double result, final Double... args) throws IOException {
        final JsonArrayBuilder arguments = Json.createArrayBuilder();
        final StringBuilder path = new StringBuilder();
        path.append('/');
        path.append(operation);
        for (final Double arg : args) {
            path.append('/');
            path.append(arg);
            arguments.add(arg);
        }
        MatcherAssert.assertThat(
            new JdkRequest(base.resolve(path.toString()))
                .header("Accept", "application/json")
                .fetch()
                .as(JsonResponse.class).json().readObject(),
            Matchers.equalTo(
                Json.createObjectBuilder()
                    .add(
                        "operation", Json.createObjectBuilder()
                            .add("name", operation)
                            .add(
                                "arguments", arguments
                            )
                            .add("result", result)
                    )
                    .build()
            )
        );
    }

    /**
     * Reserves a port.
     * @return Reserved port
     */
    private static int port() {
        try (final ServerSocket socket = new ServerSocket()) {
            socket.setReuseAddress(true);
            socket.bind(
                new InetSocketAddress(InetAddress.getLoopbackAddress(), 0)
            );
            return socket.getLocalPort();
        } catch (final IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * Creates uri to bind server to.
     * @return Uri with free port used.
     * @throws URISyntaxException In case of URI composition issues
     */
    private static URI uri() throws URISyntaxException {
        final URI uri = new URI(String.format("http://localhost:%d", MainTest.port()));
        return uri;
    }

}
