# Intro

This is REST based calculator implementation with basic operations like add, substract, divide and multiply.

The application uses cache mechanism in case same operation with same arguments was invoked more then once.

# Usage

To compile the application run `mvn clean compile assembly:single` from the terminal.
To launch the application, run `java -jar target/rest-calculator.jar --port=8080`

In order to test the application one needs to open `http://localhost:8080/<operation>/<a>/<b>/<c>?` in a browser,
where `operation` string should be substituted to one of following operations supported:
* add
* substract
* multiply
* divide

and <a>, <b> and <c> - to numbers to be used while executing arithmetical operation

**Note** that `divide` operation supports only two operand, otherwise 404 error will be raised

# Example
`http://localhost:8080/add/1/2/3`

Output
```
{
    "operation":{
        "name":"add",
        "arguments":[
            1.0,
            2.0,
            3.0
        ],
        "result":6.0
    }
}
```


# References
* As a base tool to implement REST JSON endpoint [`Takes`](https://github.com/yegor256/takes) framework was taken.
